var urlAlert = "http://localhost:3003/alert/";
var urlMeteo = "http://localhost:3001/meteo/all/";
var urlPollution = "http://localhost:3002/pollution/";
var urlDepartment = "http://vicopo.selfbuild.fr/city/";
var cityFound = false;

// Expand-Collapse Panel
$(document).on('click', '.panel-heading span.clickable', function(e){
    var $this = $(this);
	if(!$this.hasClass('panel-collapsed')) {
		$this.parents('.panel').find('.panel-body').slideUp();
		$this.addClass('panel-collapsed');
		$this.find('i').removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
	} else {
		$this.parents('.panel').find('.panel-body').slideDown();
		$this.removeClass('panel-collapsed');
		$this.find('i').removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');
	}
});



// Get Info by submit Department ID and City
$(document).ready(function(e){
    $('#sForm').on('submit',function(e){
        e.preventDefault();

        $('#formCity').removeClass('has-error');
        $('#formDepartment').addClass('has-success');

        // Get Input/Select Value
        var cityName = $('#inputCity').val();
        
        var departmentID;
        var urlDepartmentByCityName = urlDepartment.concat(cityName);
        

        

        
        
        // Set Panel Title with Input Value
        $('#panelAlertTitle').text("Weather Alert for the department " + departmentID);
        $('#panelMeteoTitle').text("Weather Info for " + cityName);
        $('#panelPollutionTitle').text("Pollution Indice for " + cityName);

        // Create API's URL from urlAlert and departmentID
        var urlMeteoByCityName = urlMeteo.concat(cityName);
        var urlPollutionByCityName = urlPollution.concat(cityName);
        var urlAlertByDepartmentID;
        
        
        // Function to get JSON pollution and Display it
        var fnPollution = function(){
            // Get JSON from urlPollutionByCityName
            $.getJSON(urlPollutionByCityName, function(data) {  
                $('#formCity').addClass('has-success');

                // Empty the tbody 
                $('#PollutionRows tr').remove();

                // Generate a row for Pollution Indice and add to table
                var $row = $('<tr>' +
                            '<td>Pollution Indice</td>' + 
                            '<td>' + data.result + '</td>' + 
                            '</tr>');
                $('#PollutionRows').append($row);
                
                // Show the table when it's filled
                $('#panelPollution').show("slow");
            }).fail(function(data){               
                 $('#formCity').removeClass('has-success').addClass('has-error'); 
                 
                // Hide the table 
                $('#panelPollution').hide();
                $('#panelMeteo').hide();

                alert("City not Found");
            });

        };

        // Function to get JSON weather and Display it
        var fnWeather = function(){
            // Get JSON from urlMeteoByCityName
            $.getJSON(urlMeteoByCityName, function(data) {  
                // Empty the tbody 
                $('#meteoRows tr').remove();
                
                // Generate a row for each information and add to table
                var $row = $('<tr>' +
                            '<td>Weather</td>' + 
                            '<td>' + data.weather[0].main + '</td>' + 
                            '</tr>' +
                            '<tr>' +                         
                            '<td>Current Temperature</td>' + 
                            '<td>' + data.main.temp + '°C</td>' + 
                            '</tr>' +
                            '<tr>' +
                            '<td>Temperature Min</td>' + 
                            '<td>' + data.main.temp_min + '°C</td>' + 
                            '</tr>' + 
                            '<tr>' +
                            '<td>Temperature Max</td>' + 
                            '<td>' + data.main.temp_max + '°C</td>' + 
                            '</tr>');
                $('#meteoRows').append($row);
                
                // Show the table when it's filled
                $('#panelMeteo').show("slow");              
                
            });            
        };

        // Function to get JSON alert and Display it
        var fnAlert = function(){

            // Get JSON from urlAlertByDepartmentID
            $.getJSON(urlDepartmentByCityName, function(data) {
                $.each(data.cities, function(index, value){
                    if(value.city.toLowerCase == cityName.toLowerCase && value.city.length == cityName.length){
                        urlAlertByDepartmentID = urlAlert.concat(value.code.toString().substring(0,2));
                        return false;
                    }
                })
            });

            // Get JSON from urlAlertByDepartmentID
            $.getJSON(urlAlertByDepartmentID, function(data) {  
                // Empty the tbody 
                $('#alertRows tr').remove();
                // Iterate to the AlertMeteo Array  
                $.each(data.AlertMeteo, function(index, value){
                    // For each phenomenes, generate a row and add to table
                    var $row = $('<tr>' +
                                '<td>' + value.phenomene + '</td>' + 
                                '<td>' + value.risque + '</td>' + 
                                '</tr>');
                    $('#alertRows').append($row);
                });
                
                // Show the table when it's filled
                $('#panelAlert').show("slow");
                
            });
        }

        

        // Check if cityName is not NULL
        if(cityName){                                  
            fnPollution();
            fnWeather();
            fnAlert();            
        }else{            
            $('#formCity').removeClass('has-success').addClass('has-error'); 

            // Hide the tables pollution and weather 
            $('#panelPollution').hide();
            $('#panelMeteo').hide();

            alert("Input Empty");
        } 
        

    });
});

