# ArchitectureNodeJs

Le projet comporte les différents micro-services :
- Météo
- Alerte météo
- Pollution
- Notification
- Vol
- Mailer
```

### Prerequis

* Un serveur Nodejs
* Un navigateur
```

### Installation / Déploiement

Une fois le serveur node installer, cloner les différents dépots.
Allez dans chaque dossier à l'aide d'une invite de commandes
Taper "npm install" pour installer les différentes dépendances
Pour le lancer soit "npm start"
```

## Construit avec

* HTML / CSS / Javascript[JQuery](https://jquery.com/download/)
* [NodeJs](https://nodejs.org/fr/) - Serveur back permettant de faire nos micro-services
* [NoSQL Cassandra](http://cassandra.apache.org/download/)

## Versioning

Nous utilisons [Bitbucket](https://bitbucket.org/) pour le versioning. Pour avoir nos différentes version, allez sur notre [dépot](https://bitbucket.org/account/user/kevcedben/projects/ANJS). 

## Auteurs
- Cédric Berniard
- Benjamin Grimault
- Kévin Lepage